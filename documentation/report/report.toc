\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Introdu\IeC {\c c}\IeC {\~a}o}{17}{chapter*.9}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}Sistema de Navega\IeC {\c c}\IeC {\~a}o Autonoma para Rob\IeC {\^o}s M\IeC {\'o}veis}{19}{chapter.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}Localiza\IeC {\c c}\IeC {\~a}o e Mapeamento via Sistema de Posicionamento Indoor com Wi-Fi}{21}{chapter.2}
\contentsline {section}{\numberline {2.1}Abordagem \emph {FingerPrinting}}{22}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Treinamento (\emph {Off-line})}{22}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}Algoritmos}{22}{section.2.2}
\contentsline {section}{\numberline {2.3}Dispositivos}{22}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Smartphone}{22}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Robotino}{22}{subsection.2.3.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}Planejamento de Trajet\IeC {\'o}ria para o Rob\IeC {\^o} M\IeC {\'o}vel}{23}{chapter.3}
\contentsline {section}{\numberline {3.1}Robotino}{23}{section.3.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}Resultados}{25}{chapter.4}
\contentsline {part}{\partnumberline {I}Prepara\IeC {\c c}\IeC {\~a}o do relat\IeC {\'o}rio}{27}{part.1}
\contentsline {part}{\partnumberline {II}Resultados}{29}{part.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}Lorem ipsum dolor sit amet}{31}{chapter.5}
\contentsline {section}{\numberline {5.1}Aliquam vestibulum fringilla lorem}{31}{section.5.1}
\vspace {\cftbeforepartskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Conclus\IeC {\~a}o}{33}{chapter*.13}
\vspace {\cftbeforepartskip }
\renewcommand *{\cftappendixname }{AP\^ENDICE \space }
\setlength {\cftpartindent }{\cftlastnumwidth } \setlength {\cftpartnumwidth }{2em}
\cftinsert {A}
\contentsline {part}{Ap\^endices}{35}{section*.15}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {A}Quisque libero justo}{37}{appendix.A}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {B}Nullam elementum urna vel imperdiet sodales elit ipsum pharetra ligula ac pretium ante justo a nulla curabitur tristique arcu eu metus}{39}{appendix.B}
\cftinsert {AAA}
\renewcommand *{\cftappendixname }{ANEXO \space }
\setlength {\cftpartindent }{\cftlastnumwidth } \setlength {\cftpartnumwidth }{2em}
\cftinsert {A}
\contentsline {part}{Anexos}{41}{section*.16}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {A}Morbi ultrices rutrum lorem.}{43}{appendix.anexochapback.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {B}Cras non urna sed feugiat cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus}{45}{appendix.anexochapback.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {C}Fusce facilisis lacinia dui}{47}{appendix.anexochapback.3}
\vspace {\cftbeforepartskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Exemplo de Formul\IeC {\'a}rio de Identifica\IeC {\c c}\IeC {\~a}o}{49}{appendix*.18}
