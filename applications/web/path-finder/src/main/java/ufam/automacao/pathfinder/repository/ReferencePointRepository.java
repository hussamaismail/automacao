package ufam.automacao.pathfinder.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import ufam.automacao.pathfinder.domain.Device;
import ufam.automacao.pathfinder.domain.ReferencePoint;

@Repository
public class ReferencePointRepository {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public ReferencePoint saveReferencePoint(ReferencePoint referencePoint) {
		entityManager.persist(referencePoint);
		return referencePoint;
	}
	
	public List<ReferencePoint> getReferencePoints() {
		TypedQuery<ReferencePoint> createQuery = entityManager.createQuery("SELECT r From ReferencePoint r",ReferencePoint.class);		
		return createQuery.getResultList();
	}
	
	public List<ReferencePoint> getReferencePointsByDevice(Device device) {
		TypedQuery<ReferencePoint> createQuery = entityManager.createQuery("SELECT r From ReferencePoint r WHERE r.device = :device",ReferencePoint.class);
		createQuery.setParameter("device", device);
		return createQuery.getResultList();
	}

}
