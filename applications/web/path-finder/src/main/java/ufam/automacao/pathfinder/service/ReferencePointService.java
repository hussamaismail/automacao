package ufam.automacao.pathfinder.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ufam.automacao.pathfinder.domain.Device;
import ufam.automacao.pathfinder.domain.ReferencePoint;
import ufam.automacao.pathfinder.repository.ReferencePointRepository;

@Service
@Transactional
public class ReferencePointService {
	
	@Autowired
	private ReferencePointRepository referencePointRepository;

	public ReferencePoint saveReferencePoint(ReferencePoint referencePoint) {
		referencePointRepository.saveReferencePoint(referencePoint);
		return referencePoint;
	}
	
	public List<ReferencePoint> getReferencePoints() {
		return referencePointRepository.getReferencePoints();		
	}
	
	public List<ReferencePoint> getVizinhosProximos(ReferencePoint onlineRP, Integer k){
		
		List<ReferencePoint> referencePoints = referencePointRepository.getReferencePointsByDevice(onlineRP.getDevice());		
		Comparator<ReferencePoint> distanceComparator = new Comparator<ReferencePoint>() {
			public int compare(ReferencePoint rp1, ReferencePoint rp2) {
				if (rp1.getDistance() < rp2.getDistance()){
					return -1;	
				}else if (rp1.getDistance() == rp2.getDistance()){
					return 0;
				}else{
					return 1;
				}				
			}
		};		
		SortedSet<ReferencePoint> vizinhosProximosOrdenados = new TreeSet<ReferencePoint>(distanceComparator);		
		double distance = 0;
		for (ReferencePoint referencePoint : referencePoints) {
			distance = 0;			
			distance = distance + Math.pow((onlineRP.getAp1() - referencePoint.getAp1() ), 2); 
			distance = distance + Math.pow((onlineRP.getAp2() - referencePoint.getAp2() ), 2);
			distance = distance + Math.pow((onlineRP.getAp3() - referencePoint.getAp3() ), 2);
			distance = distance + Math.pow((onlineRP.getAp4() - referencePoint.getAp4() ), 2);
			distance = Math.sqrt(distance);
			referencePoint.setDistance(distance);
			vizinhosProximosOrdenados.add(referencePoint);
		}		
		referencePoints.clear();
		
		Iterator<ReferencePoint> iterator = vizinhosProximosOrdenados.iterator();
		int current = 0;
		if (k == -1){
			k = vizinhosProximosOrdenados.size();
		}
		while(iterator.hasNext() && current < k ){
			ReferencePoint rp = iterator.next();
			referencePoints.add(rp);
			current++;
		}
		
		return referencePoints;
	}
	
	public List<ReferencePoint> EWKNN(ReferencePoint onlineRP){
		
		List<ReferencePoint> referencePoints = referencePointRepository.getReferencePointsByDevice(onlineRP.getDevice());		
		Comparator<ReferencePoint> distanceComparator = new Comparator<ReferencePoint>() {
			public int compare(ReferencePoint rp1, ReferencePoint rp2) {
				if (rp1.getDistance() < rp2.getDistance()){
					return -1;	
				}else if (rp1.getDistance() == rp2.getDistance()){
					return 0;
				}else{
					return 1;
				}				
			}
		};		
		
		SortedSet<ReferencePoint> rpsCandidatos = new TreeSet<ReferencePoint>(distanceComparator);		
		
		/* Calcula a distancia */
//		double distance = 0;		
//		for (ReferencePoint referencePoint : referencePoints) {
//			distance = 0;			
//			distance = distance + Math.abs(onlineRP.getAp1() - referencePoint.getAp1()); 
//			distance = distance + Math.abs(onlineRP.getAp2() - referencePoint.getAp2());
//			distance = distance + Math.abs(onlineRP.getAp3() - referencePoint.getAp3());
//			distance = distance + Math.abs(onlineRP.getAp4() - referencePoint.getAp4());
//			referencePoint.setDistance(distance);
//			rpsCandidatos.add(referencePoint);
//		}		
//		referencePoints.clear();
		
		double distance = 0;
		for (ReferencePoint referencePoint : referencePoints) {
			distance = 0;			
			distance = distance + Math.pow((onlineRP.getAp1() - referencePoint.getAp1() ), 2); 
			distance = distance + Math.pow((onlineRP.getAp2() - referencePoint.getAp2() ), 2);
			distance = distance + Math.pow((onlineRP.getAp3() - referencePoint.getAp3() ), 2);
			distance = distance + Math.pow((onlineRP.getAp4() - referencePoint.getAp4() ), 2);
			distance = Math.sqrt(distance);
			referencePoint.setDistance(distance);
			rpsCandidatos.add(referencePoint);
		}	
		referencePoints.clear();
		
		/* Aplica o limiar */
		//double rt = 3.5;
		double rt = 5.0;
		Iterator<ReferencePoint> iterator = rpsCandidatos.iterator();
		while(iterator.hasNext()){
			ReferencePoint rp = iterator.next();
			if (rp.getDistance() <= rt){
				referencePoints.add(rp);	
			}else{
				break;
			}
		}
		
		/* Filtro de Média */
		System.out.println("Tamanho: " + referencePoints.size());
		double e = 0;
		for (int i=1; i < referencePoints.size(); i++){
			e = e + (referencePoints.get(i).getDistance() - referencePoints.get(0).getDistance());
			System.out.println("i: " + i);
		}
		e = e / (referencePoints.size() - 1);		
		System.out.println("E(S) = " + e);
			
		List<ReferencePoint> rfs = new ArrayList<ReferencePoint>();				
		for (int i=1; i < referencePoints.size(); i++){
			if ((referencePoints.get(i).getDistance() - referencePoints.get(0).getDistance()) <= e){
				rfs.add(referencePoints.get(i));
			}
		}
		
		/* FILTRO P (PROVAVEIS) */
		if ((Device.ROBOTINO.equals(onlineRP.getDevice()) && (onlineRP.getX() != -1) && (onlineRP.getY() != -1 ))){			
			
			List<ReferencePoint> possiveis = rfs;
			List<ReferencePoint> provaveis = new ArrayList<ReferencePoint>();
			
			Integer last_x = onlineRP.getX();
			Integer last_y = onlineRP.getY();
			
			double trh = 120;

			System.out.println("\n\nUltima Posicao: " + last_x + ","+ last_y);
			for (ReferencePoint referencePoint : possiveis) {			
				Integer x = referencePoint.getX();
				Integer y = referencePoint.getY();				
				double delta = Math.sqrt(Math.pow(last_x - x,2) + Math.pow(last_y - y, 2))  ;
				if (delta < trh){
					provaveis.add(referencePoint);
					System.out.println("Provavel ("+ delta + "): " + x + "," + y);
				}else{
					System.out.println("Improvavel ("+ delta + "): " + x + "," + y);
				}
			}
			return provaveis;
		}

		return rfs;
	}
	
	public int[] getXYusingEWKNN(ReferencePoint onlineRP){
		
		int position[] = {0,0};
		
		List<ReferencePoint> referencePoints = EWKNN(onlineRP);
		
		if (referencePoints.size() == 0){
			position[0] = -1;
			position[1] = -1;
			return position;
		}		
		
		double acc_numerador_x = 0, acc_numerador_y = 0, acc_denominador = 0;		
		for (ReferencePoint referencePoint : referencePoints) {
			double distancia = referencePoint.getDistance() == 0 ? 1.0E-6 : referencePoint.getDistance();
			acc_numerador_x = acc_numerador_x + ((1 / distancia) * referencePoint.getX());
//			acc_numerador_x = acc_numerador_x + ((1 / distancia) * ReferencePointXY.valueOf("RP" + referencePoint.getRp()).getX());
			acc_numerador_y = acc_numerador_y + ((1 / distancia) * referencePoint.getY());
//			acc_numerador_y = acc_numerador_y + ((1 / distancia) * ReferencePointXY.valueOf("RP" + referencePoint.getRp()).getY());
			acc_denominador = acc_denominador + (1/distancia);
		}
		position[0] = (int) (acc_numerador_x / acc_denominador);
		position[1] = (int) (acc_numerador_y / acc_denominador);
		
		return position;
	}
	
	public List<ReferencePoint> getVizinhosProximosUsandoKNN(ReferencePoint onlineRP, int k){
		
		List<ReferencePoint> referencePoints = referencePointRepository.getReferencePointsByDevice(onlineRP.getDevice());		
		Comparator<ReferencePoint> distanceComparator = new Comparator<ReferencePoint>() {
			public int compare(ReferencePoint rp1, ReferencePoint rp2) {
				if (rp1.getDistance() < rp2.getDistance()){
					return -1;	
				}else if (rp1.getDistance() == rp2.getDistance()){
					return 0;
				}else{
					return 1;
				}				
			}
		};		
				
		SortedSet<ReferencePoint> rpsCandidatos = new TreeSet<ReferencePoint>(distanceComparator);		
		
		double distance = 0;
		for (ReferencePoint referencePoint : referencePoints) {
			distance = 0;			
			distance = distance + Math.pow((onlineRP.getAp1() - referencePoint.getAp1() ), 2); 
			distance = distance + Math.pow((onlineRP.getAp2() - referencePoint.getAp2() ), 2);
			distance = distance + Math.pow((onlineRP.getAp3() - referencePoint.getAp3() ), 2);
			distance = distance + Math.pow((onlineRP.getAp4() - referencePoint.getAp4() ), 2);
			distance = Math.sqrt(distance);
			referencePoint.setDistance(distance);
			rpsCandidatos.add(referencePoint);
		}	
		referencePoints.clear();
			 
		/* Aplica o limiar */
		int count_k = 0;
		Iterator<ReferencePoint> iterator = rpsCandidatos.iterator();
		while(iterator.hasNext() && count_k < k){
			ReferencePoint rp = iterator.next();
				referencePoints.add(rp);	
				count_k ++;			
		}

		return referencePoints;
	}
	
	public int[] getXYusingWKNN(ReferencePoint onlineRP, int k){		
		int position[] = {0,0};		
		List<ReferencePoint> referencePoints = getVizinhosProximosUsandoKNN(onlineRP, k);
		if (referencePoints.size() == 0){
			position[0] = -1;
			position[1] = -1;
			return position;
		}				
		double acc_numerador_x = 0, acc_numerador_y = 0, acc_denominador = 0;		
		for (ReferencePoint referencePoint : referencePoints) {
			double distancia = referencePoint.getDistance() == 0 ? 1.0E-6 : referencePoint.getDistance();
			acc_numerador_x = acc_numerador_x + ((1 / distancia) * referencePoint.getX());
			acc_numerador_y = acc_numerador_y + ((1 / distancia) * referencePoint.getY());
			acc_denominador = acc_denominador + (1/distancia);
		}
		position[0] = (int) (acc_numerador_x / acc_denominador);
		position[1] = (int) (acc_numerador_y / acc_denominador);		
		return position;
	}

}
