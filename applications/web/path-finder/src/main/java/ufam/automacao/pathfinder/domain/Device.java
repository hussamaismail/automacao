package ufam.automacao.pathfinder.domain;

public enum Device {

	SMARTPHONE, ROBOTINO;
}
