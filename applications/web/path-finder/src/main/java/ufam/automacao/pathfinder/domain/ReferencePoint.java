package ufam.automacao.pathfinder.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "reference_points")
public class ReferencePoint {

	@Column(nullable = false)
	private Integer ap1;

	@Column(nullable = false)
	private Integer ap2;

	@Column(nullable = false)
	private Integer ap3;

	@Column(nullable = false)
	private Integer ap4;

	@Id
	@Column(nullable = false)
	private Date date;

	@Transient
	private Double distance;

	@Column(nullable = false)
	private Integer x;

	@Column(nullable = false)
	private Integer y;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Device device;
	
	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public Integer getAp1() {
		return ap1;
	}

	public void setAp1(Integer ap1) {
		this.ap1 = ap1;
	}

	public Integer getAp2() {
		return ap2;
	}

	public void setAp2(Integer ap2) {
		this.ap2 = ap2;
	}

	public Integer getAp3() {
		return ap3;
	}

	public void setAp3(Integer ap3) {
		this.ap3 = ap3;
	}

	public Integer getAp4() {
		return ap4;
	}

	public void setAp4(Integer ap4) {
		this.ap4 = ap4;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	@Override
	public String toString() {
		return "ReferencePoint [x=" + x + ", y=" + y + ", ap1=" + ap1 + ", ap2=" + ap2
				+ ", ap3=" + ap3 + ", ap4=" + ap4 + ", date=" + date
				+ ", distance=" + distance + "]";
	}

}
