package ufam.automacao.pathfinder.domain;

public enum ReferencePointXY {

	RP1(0,0 + ReferencePointXY.RODAPE),    RP2(0,30),   RP3(0,60),    RP4(0,90),    RP5(0,120),    RP6(0,150),    RP7(0,180),    RP8(0,210),    RP9(0,240),    RP10(0,270),   RP11(0,300),
	RP12(30,0 + ReferencePointXY.RODAPE),  RP13(30,30), RP14(30,60),  RP15(30,90),  RP16(30,120),  RP17(30,150),  RP18(30,180),  RP19(30,210),  RP20(30,240),  RP21(30,270),  RP22(30,300),
	RP23(60,0 + ReferencePointXY.RODAPE),  RP24(60,30),  RP25(60,60),  RP26(60,90),  RP27(60,120),  RP28(60,150),  RP29(60,180),  RP30(60,210),  RP31(60,240),  RP32(60,270),  RP33(60,300),
	RP34(90,0 + ReferencePointXY.RODAPE),  RP35(90,30),  RP36(90,60),  RP37(90,90),  RP38(90,120),  RP39(90,150),  RP40(90,180),  RP41(90,210),  RP42(90,240),  RP43(90,270),  RP44(90,300),
	RP45(120,0 + ReferencePointXY.RODAPE), RP46(120,30), RP47(120,60), RP48(120,90), RP49(120,120), RP50(120,150), RP51(120,180), RP52(120,210), RP53(120,240), RP54(120,270), RP55(120,300),
	RP56(150,0 + ReferencePointXY.RODAPE), RP57(150,30), RP58(150,60), RP59(150,90), RP60(150,120), RP61(150,150), RP62(150,180), RP63(150,210), RP64(150,240), RP65(150,270), RP66(150,300),
	
	RP67(210,30),    RP68(210,90),   RP69(210,150),    RP70(210,210),    RP71(210,270),    RP72(210,330),
	RP73(270,30),    RP74(270,90),   RP75(270,150),    RP76(270,210),    RP77(270,270),    RP78(270,330),
	RP79(330,30),    RP80(330,90),   RP81(330,150),    RP82(330,210),    RP83(330,270),
	
	RP84(240,120), RP85(240,180), RP86(240,240),
	RP87(300,120), RP88(300,180), RP89(300,240);
	
	private int x;
	private int y;
	public static final int RODAPE = 2;
	
	ReferencePointXY(Integer x, Integer y){
		this.x = x;
		this.y = y;
	}
	
	public Integer getX(){
		return this.x;
	}
	
	public Integer getY(){
		return this.y;
	}
	
}
