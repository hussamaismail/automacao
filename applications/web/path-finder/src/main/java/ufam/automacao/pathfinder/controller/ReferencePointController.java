package ufam.automacao.pathfinder.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ufam.automacao.pathfinder.domain.Device;
import ufam.automacao.pathfinder.domain.ReferencePoint;
import ufam.automacao.pathfinder.service.ReferencePointService;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

@Controller
@RequestMapping("/reference-point")
public class ReferencePointController {

	@Autowired
	private ReferencePointService referencePointService;
	
	private ReferencePoint online;	
	private ReferencePoint robotinoOnline;	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	
	public static final String IP_SMARTPHONE = "172.26.201.1";
	public static final Integer PORT_SMARTPHONE = 8080;
	
	
	public ReferencePointController() {
		super();
		online = new ReferencePoint();
		online.setDevice(Device.SMARTPHONE);
		robotinoOnline = new ReferencePoint();
		robotinoOnline.setDevice(Device.ROBOTINO);
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> getAllReferencePoints() {

		List<ReferencePoint> referencePoints = referencePointService
				.getReferencePoints();
		String json = new JSONSerializer().exclude("class").serialize(
				referencePoints);

		return new ResponseEntity<String>(json, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, headers = { "application/json" })
	public ResponseEntity<String> saveReferencePoint(@RequestBody String json) {
		ReferencePoint referencePoint = new JSONDeserializer<ReferencePoint>()
				.deserialize(json, ReferencePoint.class);
		referencePoint.setDate(Calendar.getInstance().getTime());
		System.out.println(referencePoint.toString());
		referencePointService.saveReferencePoint(referencePoint);
		return new ResponseEntity<String>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/neighbors", method = RequestMethod.GET, headers = { "application/json" })
	@ResponseBody
	public ResponseEntity<String> getVizinhosProximosJSON(
			@RequestParam(value = "k") Integer k, @RequestBody String json) {

		ReferencePoint onlineRP = new JSONDeserializer<ReferencePoint>()
				.deserialize(json, ReferencePoint.class);

		List<ReferencePoint> vizinhosProximos = referencePointService
				.getVizinhosProximos(onlineRP, k);

		String jsonOut = new JSONSerializer().exclude("class").serialize(
				vizinhosProximos);
		return new ResponseEntity<String>(jsonOut, HttpStatus.OK);
	}

	@RequestMapping(value = "/neighbors", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> getVizinhosProximos(
			@RequestParam(value = "k") Integer k,
			@RequestParam("ap1") Integer ap1, @RequestParam("ap2") Integer ap2,
			@RequestParam("ap3") Integer ap3, @RequestParam("ap4") Integer ap4) {

		ReferencePoint onlineRP = new ReferencePoint();
		onlineRP.setAp1(ap1);
		onlineRP.setAp2(ap2);
		onlineRP.setAp3(ap3);
		onlineRP.setAp4(ap4);

		List<ReferencePoint> vizinhosProximos = referencePointService
				.getVizinhosProximos(onlineRP, k);

		String jsonOut = new JSONSerializer().exclude("class").serialize(
				vizinhosProximos);
		return new ResponseEntity<String>(jsonOut, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/neighbors", params={"EWKNN"}, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> getVizinhosProximosEWKNN(
			@RequestParam("ap1") Integer ap1, @RequestParam("ap2") Integer ap2,
			@RequestParam("ap3") Integer ap3, @RequestParam("ap4") Integer ap4) {

		ReferencePoint onlineRP = new ReferencePoint();
		onlineRP.setAp1(ap1);
		onlineRP.setAp2(ap2);
		onlineRP.setAp3(ap3);
		onlineRP.setAp4(ap4);

		int[] xYusingEWKNN = referencePointService
				.getXYusingEWKNN(onlineRP);

		String jsonOut = new JSONSerializer().exclude("class").serialize(
				xYusingEWKNN);
		return new ResponseEntity<String>(jsonOut, HttpStatus.OK);
	}	

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<String> saveReferencePointForm(
			@RequestParam("x") Integer x, @RequestParam("y") Integer y, @RequestParam("ap1") Integer ap1,
			@RequestParam("ap2") Integer ap2, @RequestParam("ap3") Integer ap3,
			@RequestParam("ap4") Integer ap4, @RequestParam("device") String device) {

		ReferencePoint referencePoint = new ReferencePoint();
		referencePoint.setX(x);
		referencePoint.setY(y);
		referencePoint.setAp1(ap1);
		referencePoint.setAp2(ap2);
		referencePoint.setAp3(ap3);
		referencePoint.setAp4(ap4);
		referencePoint.setDate(Calendar.getInstance().getTime());
		
		if ("SMARTPHONE".equals(device)){
			referencePoint.setDevice(Device.SMARTPHONE);
		}else{
			referencePoint.setDevice(Device.ROBOTINO);
		}
			
		referencePointService.saveReferencePoint(referencePoint);

		return new ResponseEntity<String>(HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/online", method = RequestMethod.POST)
	public ResponseEntity<String> saveReferencePointForm(@RequestParam("ap1") Integer ap1,
			@RequestParam("ap2") Integer ap2, @RequestParam("ap3") Integer ap3,
			@RequestParam("ap4") Integer ap4, @RequestParam("device") String device ) {

		if ("ROBOTINO".equals(device)){
			robotinoOnline.setAp1(ap1);
			robotinoOnline.setAp2(ap2);
			robotinoOnline.setAp3(ap3);
			robotinoOnline.setAp4(ap4);
			robotinoOnline.setDate(Calendar.getInstance().getTime());	
		}else{
			online.setAp1(ap1);
			online.setAp2(ap2);
			online.setAp3(ap3);
			online.setAp4(ap4);
			online.setDate(Calendar.getInstance().getTime());
		}
		
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@SuppressWarnings("resource")
	@RequestMapping(value="/online", params={"smartphone"}, method = RequestMethod.GET)
	@ResponseBody
	public String getSmartphonePosition() throws IOException{
		
		InputStream is = new Socket(IP_SMARTPHONE, PORT_SMARTPHONE).getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String message = br.readLine();
		
		System.out.println(" - Message received from the server : " + message);
		
		online.setAp1(Integer.valueOf(message.split(",")[0]));
		online.setAp2(Integer.valueOf(message.split(",")[1]));
		online.setAp3(Integer.valueOf(message.split(",")[2]));
		online.setAp4(Integer.valueOf(message.split(",")[3]));
		online.setDevice(Device.SMARTPHONE);
			
		int[] xy = referencePointService.getXYusingEWKNN(online);
//		int[] xy = referencePointService.getXYusingWKNN(rf,1);
		
		return xy[0] + "," + xy[1];
	}
	
	@RequestMapping(value="/online", params={"robotino","smartphone"}, method = RequestMethod.GET)
	@ResponseBody
	public String getRobotinoAndSmartphonePosition(@RequestParam("ap1") Integer ap1,
			@RequestParam("ap2") Integer ap2, @RequestParam("ap3") Integer ap3,
			@RequestParam("ap4") Integer ap4,
			@RequestParam("last_x") Integer lastX, @RequestParam("last_y") Integer lastY) throws IOException{
		
		robotinoOnline.setAp1(ap1);
		robotinoOnline.setAp2(ap2);
		robotinoOnline.setAp3(ap3);
		robotinoOnline.setAp4(ap4);		
		robotinoOnline.setDevice(Device.ROBOTINO);
		
		/* armazena a ultima posicao */
		robotinoOnline.setX(lastX);
		robotinoOnline.setY(lastY);
			
		int[] xyr = referencePointService.getXYusingEWKNN(robotinoOnline);

		@SuppressWarnings("resource")
		InputStream is = new Socket(IP_SMARTPHONE, PORT_SMARTPHONE).getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String message = br.readLine();
				
		online.setAp1(Integer.valueOf(message.split(",")[0]));
		online.setAp2(Integer.valueOf(message.split(",")[1]));
		online.setAp3(Integer.valueOf(message.split(",")[2]));
		online.setAp4(Integer.valueOf(message.split(",")[3]));
		online.setDevice(Device.SMARTPHONE);
			
		int[] xys = referencePointService.getXYusingWKNN(online, 1);
		
//		int[] xys = {360,120};
		
		return xyr[0]+","+ xyr[1] + ";" + xys[0] + "," + xys[1] ;
	}
	
	@RequestMapping(value="/online", params={"robotino"}, method = RequestMethod.GET)
	@ResponseBody
	public String getRobotinoPosition(@RequestParam("ap1") Integer ap1,
			@RequestParam("ap2") Integer ap2, @RequestParam("ap3") Integer ap3,
			@RequestParam("ap4") Integer ap4) throws IOException{
				
		robotinoOnline.setAp1(ap1);
		robotinoOnline.setAp2(ap2);
		robotinoOnline.setAp3(ap3);
		robotinoOnline.setAp4(ap4);		
		robotinoOnline.setDevice(Device.ROBOTINO);
			
		int[] xy = referencePointService.getXYusingEWKNN(robotinoOnline);
//		int[] xy = referencePointService.getXYusingWKNN(rf,1);
		
		return xy[0] + "," + xy[1];
	}
	
	@SuppressWarnings("resource")
	@RequestMapping(value="/online", params={"painel"},  method = RequestMethod.GET)
	@ResponseBody
	public String getReferencePoint(@RequestParam(required=false, value="algoritmo") String algoritmo, @RequestParam(required=false, value="k") Integer k, @RequestParam(value="device") String device) throws Exception, IOException {
				
		String html = "";
		
		if ("SMARTPHONE".equals(device)){
			InputStream is = new Socket(IP_SMARTPHONE, PORT_SMARTPHONE).getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String message = br.readLine();
			
			online.setAp1(Integer.valueOf(message.split(",")[0]));
			online.setAp2(Integer.valueOf(message.split(",")[1]));
			online.setAp3(Integer.valueOf(message.split(",")[2]));
			online.setAp4(Integer.valueOf(message.split(",")[3]));
			online.setDevice(Device.SMARTPHONE);
		}
				
		if (algoritmo != null && algoritmo.equals("EWKNN")){
			html = html + "<html><head><meta http-equiv=\"refresh\" content=\"0.5;URL=online?painel&algoritmo="+algoritmo+"&device=" + device + "\"></head><body><center>";
		}else if (algoritmo != null && algoritmo.equals("WKNN")){
			html = html + "<html><head><meta http-equiv=\"refresh\" content=\"0.5;URL=online?painel&algoritmo="+algoritmo+"&k="+k+"&device=" + device + "\"></head><body><center>";
		}
		if ("ROBOTINO".equals(device)){
			html = html + "<table border=\"1\">";
			html = html + "<tr><td colspan='2' align=\"center\"><b>Coletas de Sinal</b></td></tr>";
			html = html + "<tr> <td align=\"center\" style=\"width: 100px\">AP1:</td> <td align=\"center\" style=\"width: 100px\">" + robotinoOnline.getAp1() + " </td></tr>";
			html = html + "<tr> <td align=\"center\" style=\"width: 100px\">AP2:</td> <td align=\"center\" style=\"width: 100px\">" + robotinoOnline.getAp2() + " </td></tr>";
			html = html + "<tr> <td align=\"center\" style=\"width: 100px\">AP3:</td> <td align=\"center\" style=\"width: 100px\">" + robotinoOnline.getAp3() + " </td></tr>";
			html = html + "<tr> <td align=\"center\" style=\"width: 100px\">AP4:</td> <td align=\"center\" style=\"width: 100px\">" + robotinoOnline.getAp4() + " </td></tr>";
			html = html + "</table>";		
			
			html = html + "<br><table border=\"1\"><tr><td style=\"width: 250px\" align=\"center\"><b>X</b></td><td style=\"width: 250px\" align=\"center\"><b>Y</b></td></tr>";
			html = html + "<tr><td align=\"center\"><font size=\"25\">"+ robotinoOnline.getX() + "</font></td><td align=\"center\"><font size=\"25\">"+ robotinoOnline.getY() +"</font></td></tr>";
			html = html + "</table>";
		}else{		
			html = html + "<table border=\"1\">";
			html = html + "<tr><td colspan='2' align=\"center\"><b>Coletas de Sinal</b></td></tr>";
			html = html + "<tr> <td align=\"center\" style=\"width: 100px\">AP1:</td> <td align=\"center\" style=\"width: 100px\">" + online.getAp1() + " </td></tr>";
			html = html + "<tr> <td align=\"center\" style=\"width: 100px\">AP2:</td> <td align=\"center\" style=\"width: 100px\">" + online.getAp2() + " </td></tr>";
			html = html + "<tr> <td align=\"center\" style=\"width: 100px\">AP3:</td> <td align=\"center\" style=\"width: 100px\">" + online.getAp3() + " </td></tr>";
			html = html + "<tr> <td align=\"center\" style=\"width: 100px\">AP4:</td> <td align=\"center\" style=\"width: 100px\">" + online.getAp4() + " </td></tr>";
			html = html + "</table>";		
			
			html = html + "<br><table border=\"1\"><tr><td style=\"width: 250px\" align=\"center\"><b>X</b></td><td style=\"width: 250px\" align=\"center\"><b>Y</b></td></tr>";
			html = html + "<tr><td align=\"center\"><font size=\"25\">"+ online.getX() + "</font></td><td align=\"center\"><font size=\"25\">"+ online.getY() +"</font></td></tr>";
			html = html + "</table>";
		}
			
		if (!("ROBOTINO".equals(device) && (online.getAp1() != null & online.getAp2() != null && online.getAp3() != null && online.getAp4() != null)) || (("ROBOTINO".equals(device) && (robotinoOnline.getAp1() != null & robotinoOnline.getAp2() != null && robotinoOnline.getAp3() != null && robotinoOnline.getAp4() != null)))){
			
			if (algoritmo != null && algoritmo.equals("EWKNN")){
				List<ReferencePoint> ewknn = null;
				if ("ROBOTINO".equals(device)){
					ewknn = referencePointService.EWKNN(robotinoOnline);
				}else{
					ewknn = referencePointService.EWKNN(online);
				}
				html = html + "<br><table border=\"1\"><tr><td style=\"width: 250px\" align=\"center\"><b>Vizinhos Próximos</b></td><td style=\"width: 250px\" align=\"center\"><b>Distância</b></td></tr>";
				for (ReferencePoint referencePoint : ewknn) {
					html = html + "<tr><td align=\"center\">"+ referencePoint.getX() + ","+referencePoint.getY()+"</td><td align=\"center\">"+ referencePoint.getDistance() +"</td></tr>";
				}
				html = html + "</table>";

				
				if ("ROBOTINO".equals(device)){
					int[] xy = referencePointService.getXYusingEWKNN(robotinoOnline);
					robotinoOnline.setX(xy[0]);
					robotinoOnline.setY(xy[1]);
				}else{
					int[] xy = referencePointService.getXYusingEWKNN(online);
					online.setX(xy[0]);
					online.setY(xy[1]);
				}
				
			}else if (algoritmo != null && algoritmo.equals("WKNN")){
				List<ReferencePoint> knn = null;
				if ("ROBOTINO".equals(device)){
					knn = referencePointService.getVizinhosProximosUsandoKNN(robotinoOnline,k);
				}else{
					knn = referencePointService.getVizinhosProximosUsandoKNN(online,k);
				}
				html = html + "<br><table border=\"1\"><tr><td style=\"width: 250px\" align=\"center\"><b>Vizinhos Próximos (x,y)</b></td><td style=\"width: 250px\" align=\"center\"><b>Distância</b></td><td><b>Data da Coleta</b></td></tr>";
				for (ReferencePoint referencePoint : knn) {
					html = html + "<tr><td align=\"center\">"+ referencePoint.getX() + ","+referencePoint.getY()+"</td><td align=\"center\">"+ referencePoint.getDistance() +"</td><td>" + sdf.format(referencePoint.getDate()) + "</td></tr>";
				}
				html = html + "</table>";
								
				if ("ROBOTINO".equals(device)){
					int[] xy = referencePointService.getXYusingWKNN(robotinoOnline,k);
					robotinoOnline.setX(xy[0]);
					robotinoOnline.setY(xy[1]);
				}else{
					int[] xy = referencePointService.getXYusingWKNN(online,k);
					online.setX(xy[0]);
					online.setY(xy[1]);
				}							
			}
		}				
					
		html = html + "</center></body></html>";		
		return html;
	}
	
}
