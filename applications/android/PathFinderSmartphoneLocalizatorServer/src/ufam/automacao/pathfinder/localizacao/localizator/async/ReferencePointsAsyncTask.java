package ufam.automacao.pathfinder.localizacao.localizator.async;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import ufam.automacao.pathfinder.localizacao.localizator.activity.PrincipalActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;

public class ReferencePointsAsyncTask extends AsyncTask<Context, Void, Void> {
	
	private WifiManager wifiManager;
	private Context context;
	private boolean finished;
	
	public ReferencePointsAsyncTask(WifiManager wifiManager, Context context) {
		super();
		this.wifiManager = wifiManager;
		this.context = context;
	}

	@Override
	protected Void doInBackground(Context... params) {
				
		while(PrincipalActivity.AP1_SIGNAL == null || PrincipalActivity.AP2_SIGNAL == null || PrincipalActivity.AP3_SIGNAL == null || PrincipalActivity.AP4_SIGNAL == null){
			finished = false;
			
			BroadcastReceiver bcr = new BroadcastReceiver() {
				@Override
				public void onReceive(Context c, Intent intent) {
					
					List<ScanResult> results = wifiManager.getScanResults();
					for (ScanResult s : results) {
						if ((s.SSID.equals(PrincipalActivity.AP1)) || (s.SSID.equals(PrincipalActivity.AP2))
								|| (s.SSID.equals(PrincipalActivity.AP3)) || (s.SSID.equals(PrincipalActivity.AP4))) {
							if (s.SSID.equals(PrincipalActivity.AP1)) {
								PrincipalActivity.AP1_SIGNAL = s.level;
							} else if (s.SSID.equals(PrincipalActivity.AP2)) {
								PrincipalActivity.AP2_SIGNAL = s.level;
							} else if (s.SSID.equals(PrincipalActivity.AP3)) {
								PrincipalActivity.AP3_SIGNAL = s.level;
							} else if (s.SSID.equals(PrincipalActivity.AP4)) {
								PrincipalActivity.AP4_SIGNAL = s.level;
							}
						}
					}
					finished = true;
					PrincipalActivity.instance.unregisterReceiver(this);
				}			
			};
			
			context.registerReceiver(bcr,new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
			wifiManager.startScan();
			
			while(finished == false){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {		

		try {
			OutputStream outputStream = PrincipalActivity.socket.getOutputStream();
			String retorno = String.valueOf(PrincipalActivity.AP1_SIGNAL) + "," + String.valueOf(PrincipalActivity.AP2_SIGNAL) + "," +String.valueOf(PrincipalActivity.AP3_SIGNAL) + "," +String.valueOf(PrincipalActivity.AP4_SIGNAL); 
			outputStream.write(retorno.getBytes());
			outputStream.close();		
			PrincipalActivity.socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try{
			ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
			toneG.startTone(ToneGenerator.TONE_CDMA_ONE_MIN_BEEP, 500);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		PrincipalActivity.reset();
		PrincipalActivity.serverSocketThread.run();		
		super.onPostExecute(result);
	}

}
