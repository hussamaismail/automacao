package ufam.automacao.pathfinder.localizacao.localizator.activity;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import ufam.automacao.pathfinder.localizacao.localizator.R;
import ufam.automacao.pathfinder.localizacao.localizator.async.ReferencePointsAsyncTask;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PrincipalActivity extends Activity {

	public static final String AP1 = "R-TOP";
	public static final String AP2 = "R-BOTTOM";
	public static final String AP3 = "R-LEFT";
	public static final String AP4 = "R-RIGHT";
	
//	public static final String AP1 = "appCETELI";
//	public static final String AP2 = "AmazoniaVirtual";
//	public static final String AP3 = "CETELI_W01";
//	public static final String AP4 = "CETELI_W03";

	public static Integer AP1_SIGNAL;
	public static Integer AP2_SIGNAL;
	public static Integer AP3_SIGNAL;
	public static Integer AP4_SIGNAL;

	public static PrincipalActivity instance;
	public static WifiManager wifiManager;

	public static ServerSocket serverSocket;	
	public static ServerSocketThread serverSocketThread;
	public static Socket socket;
	
	@SuppressLint("DefaultLocale") @Override
	protected void onCreate(Bundle savedInstanceState) {

		setContentView(R.layout.principal_activity);
		instance = this;
		wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		serverSocketThread = new ServerSocketThread();

		try {
			serverSocket = new ServerSocket(8080);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		TextView tvIP = (TextView) PrincipalActivity.instance.findViewById(R.id.ip);
		String ipString = String.format(
				   "%d.%d.%d.%d",
				   (wifiManager.getConnectionInfo().getIpAddress() & 0xff),
				   (wifiManager.getConnectionInfo().getIpAddress() >> 8 & 0xff),
				   (wifiManager.getConnectionInfo().getIpAddress() >> 16 & 0xff),
				   (wifiManager.getConnectionInfo().getIpAddress() >> 24 & 0xff));				   
		tvIP.setText("IP: " + ipString + ":" + serverSocket.getLocalPort() + " (Socket TCP)");
		
		super.onCreate(savedInstanceState);
	}

	public void iniciarServidorSocket(View view){
		Button button = (Button) findViewById(R.id.botao);
		button.setText("Em Execução");
		button.setEnabled(false);
		serverSocketThread.run();
	}
	
	public static void acceptConnection() throws IOException{
		socket = PrincipalActivity.serverSocket.accept();
		PrincipalActivity.getInformations();
	}
	
	public static void getInformations() throws IOException{
		ReferencePointsAsyncTask rpat = new ReferencePointsAsyncTask(
		PrincipalActivity.wifiManager,
		PrincipalActivity.instance);
		rpat.execute();
	}

	@Override
	protected void onDestroy() {
		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		super.onDestroy();
	}
	
	public static void reset(){
		PrincipalActivity.AP1_SIGNAL = null;
		PrincipalActivity.AP2_SIGNAL = null;
		PrincipalActivity.AP3_SIGNAL = null;
		PrincipalActivity.AP4_SIGNAL = null;
	}

	public class ServerSocketThread extends Thread {
		@Override
		public void run() {				
			try {				
				PrincipalActivity.acceptConnection();
			} catch (IOException e) {
				e.printStackTrace();
			}				
		}
	}
}
