package ufam.automacao.pathfinder.localizacao.domain;

public class ReferencePoint {

	private Integer x;
	private Integer y;
	private Integer ap1;
	private Integer ap2;
	private Integer ap3;
	private Integer ap4;

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public Integer getAp1() {
		return ap1;
	}

	public void setAp1(Integer ap1) {
		this.ap1 = ap1;
	}

	public Integer getAp2() {
		return ap2;
	}

	public void setAp2(Integer ap2) {
		this.ap2 = ap2;
	}

	public Integer getAp3() {
		return ap3;
	}

	public void setAp3(Integer ap3) {
		this.ap3 = ap3;
	}

	public Integer getAp4() {
		return ap4;
	}

	public void setAp4(Integer ap4) {
		this.ap4 = ap4;
	}

}
