package ufam.automacao.pathfinder.localizacao.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import ufam.automacao.pathfinder.localizacao.R;
import ufam.automacao.pathfinder.localizacao.async.ReferencePointsAsyncTask;
import ufam.automacao.pathfinder.localizacao.domain.ReferencePoint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class PrincipalActivity extends Activity {

	public static final String AP1 = "R-TOP";
	public static final String AP2 = "R-BOTTOM";
	public static final String AP3 = "R-LEFT";
	public static final String AP4 = "R-RIGHT";

//	public static final String AP1 = "AmazoniaVirtual";
//	public static final String AP2 = "CETELI_W03";
//	public static final String AP3 = "CETELI_W01";
//	public static final String AP4 = "CETELI_W05";
	
	public static Integer QTD_COLETA = 20;
	public static List<ReferencePoint> lista = new LinkedList<ReferencePoint>();

	public static final String SERVER = "http://10.0.0.112:8080/path-finder/web/reference-point";
//	public static final String SERVER = "http://192.168.0.107:8080/path-finder/web/reference-point";
	public static Integer counter = 0;
	public static Activity current;

	private SimpleDateFormat format = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss");
	private Date dataAtualizacao;

	public WifiManager wifiManager;
	public static PrincipalActivity instance;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		instance = this;

		setContentView(R.layout.principal_activity);
		TextView tv = (TextView) findViewById(R.id.ap1_ssid);
		tv.setText(AP1);

		TextView tv2 = (TextView) findViewById(R.id.ap2_ssid);
		tv2.setText(AP2);

		TextView tv3 = (TextView) findViewById(R.id.ap3_ssid);
		tv3.setText(AP3);

		TextView tv4 = (TextView) findViewById(R.id.ap4_ssid);
		tv4.setText(AP4);

		String rps[] = new String[30];
		for (int i = 0; i < 30; i++) {
			rps[i] = String.valueOf(30 * i);
		}
		/*
		 * rps[0] = "1"; rps[1] = "2"; rps[2] = "3"; rps[3] = "4";
		 */

		Spinner sx = (Spinner) findViewById(R.id.x);
		ArrayAdapter adapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, rps);
		sx.setAdapter(adapter);
		
		Spinner sy = (Spinner) findViewById(R.id.y);
		sy.setAdapter(adapter);

		// String height[] = new String[8];
		// height[0] = "0";
		// height[1] = "15";
		// height[2] = "30";
		// height[3] = "45";
		// height[4] = "60";
		// height[5] = "75";
		// height[6] = "90";
		// height[7] = "105";
		//
		// Spinner s2 = (Spinner) findViewById(R.id.height);
		// ArrayAdapter adp2 = new ArrayAdapter(this,
		// android.R.layout.simple_spinner_item, height);
		// s2.setAdapter(adp2);

		wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

		current = this;

		super.onCreate(savedInstanceState);
	}

	public void resetRSS() {
		TextView ap1_rss = (TextView) PrincipalActivity.current
				.findViewById(R.id.ap1_rss);
		ap1_rss.setText("-");
		TextView ap2_rss = (TextView) PrincipalActivity.current
				.findViewById(R.id.ap2_rss);
		ap2_rss.setText("-");
		TextView ap3_rss = (TextView) PrincipalActivity.current
				.findViewById(R.id.ap3_rss);
		ap3_rss.setText("-");
		TextView ap4_rss = (TextView) PrincipalActivity.current
				.findViewById(R.id.ap4_rss);
		ap4_rss.setText("-");
	}

	public void atualizarRSS(View v) {
		
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		BroadcastReceiver bdr = new BroadcastReceiver() {
			@Override
			public void onReceive(Context c, Intent intent) {

				resetRSS();

				TextView ap1_rss = (TextView) PrincipalActivity.current.findViewById(R.id.ap1_rss);
				TextView ap2_rss = (TextView) PrincipalActivity.current
						.findViewById(R.id.ap2_rss);
				TextView ap3_rss = (TextView) PrincipalActivity.current
						.findViewById(R.id.ap3_rss);
				TextView ap4_rss = (TextView) PrincipalActivity.current
						.findViewById(R.id.ap4_rss);

				List<ScanResult> results = wifiManager.getScanResults();
				for (ScanResult s : results) {

					if ((s.SSID.equals(AP1)) || (s.SSID.equals(AP2))
							|| (s.SSID.equals(AP3)) || (s.SSID.equals(AP4))) {

						if (s.SSID.equals(AP1)) {
							ap1_rss.setText(String.valueOf(s.level));
						} else if (s.SSID.equals(AP2)) {
							ap2_rss.setText(String.valueOf(s.level));
						} else if (s.SSID.equals(AP3)) {
							ap3_rss.setText(String.valueOf(s.level));
						} else if (s.SSID.equals(AP4)) {
							ap4_rss.setText(String.valueOf(s.level));
						}
						Button button = (Button) findViewById(R.id.atualizarRSS_bb);
						dataAtualizacao = Calendar.getInstance().getTime();
						button.setText("Atualizar ("
								+ format.format(dataAtualizacao) + ")");
						
						ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
						toneG.startTone(ToneGenerator.TONE_CDMA_ONE_MIN_BEEP, 500);
					}
				}
				
				PrincipalActivity.instance.unregisterReceiver(this);
			}
		};
		
		registerReceiver(bdr, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
		wifiManager.startScan();
	}

	public void atualizarAutomatico() {
		registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context c, Intent intent) {
				Spinner rx = (Spinner) PrincipalActivity.current.findViewById(R.id.x);
				Spinner ry = (Spinner) PrincipalActivity.current.findViewById(R.id.y);
				ReferencePoint rp = new ReferencePoint();
				rp.setX((Integer)rx.getSelectedItem());
				rp.setY((Integer)ry.getSelectedItem());
				List<ScanResult> results = wifiManager.getScanResults();
				for (ScanResult s : results) {
					if ((s.SSID.equals(AP1)) || (s.SSID.equals(AP2))
							|| (s.SSID.equals(AP3)) || (s.SSID.equals(AP4))) {
						if (s.SSID.equals(AP1)) {
							rp.setAp1(s.level);
						} else if (s.SSID.equals(AP2)) {
							rp.setAp2(s.level);
						} else if (s.SSID.equals(AP3)) {
							rp.setAp3(s.level);
						} else if (s.SSID.equals(AP4)) {
							rp.setAp4(s.level);
						}
					}
				}
				PrincipalActivity.lista.add(rp);
			}
		}, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
		wifiManager.startScan();
	}

	public static void concluir() {
		if (counter == QTD_COLETA) {
			PrincipalActivity.instance.gravar();
			counter = 0;
		} else {
			Toast.makeText(PrincipalActivity.current,
					"Obtendo " + (counter + 1) + " amostra de sinal.",
					Toast.LENGTH_SHORT).show();
			ReferencePointsAsyncTask rpat = new ReferencePointsAsyncTask(
					PrincipalActivity.instance.wifiManager,
					PrincipalActivity.current);
			rpat.execute();
		}
	}

	public void gravarBaseAutomatico(View v) {

		for (int i = 0; i < 5; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		Toast.makeText(PrincipalActivity.current,
				"Coleta automatica do RP Iniciada!", Toast.LENGTH_LONG).show();
		ReferencePointsAsyncTask rpat = new ReferencePointsAsyncTask(
				PrincipalActivity.instance.wifiManager,
				PrincipalActivity.current);
		rpat.execute();
	}

	public void gravarBase(View v) {

		TextView ap1_rss = (TextView) PrincipalActivity.current
				.findViewById(R.id.ap1_rss);
		TextView ap2_rss = (TextView) PrincipalActivity.current
				.findViewById(R.id.ap2_rss);
		TextView ap3_rss = (TextView) PrincipalActivity.current
				.findViewById(R.id.ap3_rss);
		TextView ap4_rss = (TextView) PrincipalActivity.current
				.findViewById(R.id.ap4_rss);

		if ((ap1_rss.getText().toString().equals("-"))
				|| (ap2_rss.getText().toString().equals("-"))
				|| (ap3_rss.getText().toString().equals("-"))
				|| (ap4_rss.getText().toString().equals("-"))) {
			Toast.makeText(
					PrincipalActivity.current.getApplicationContext(),
					"Existem RP's não coletados, por favor, atualize os dados!",
					Toast.LENGTH_LONG).show();
			return;
		}
		
		Spinner rx = (Spinner) PrincipalActivity.current
				.findViewById(R.id.x);
		Spinner ry = (Spinner) PrincipalActivity.current
				.findViewById(R.id.y);
		
		// Spinner height = (Spinner) PrincipalActivity.current
		// .findViewById(R.id.height);

		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(SERVER);

		try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("ap1", ap1_rss.getText()
					.toString()));
			nameValuePairs.add(new BasicNameValuePair("ap2", ap2_rss.getText()
					.toString()));
			nameValuePairs.add(new BasicNameValuePair("ap3", ap3_rss.getText()
					.toString()));
			nameValuePairs.add(new BasicNameValuePair("ap4", ap4_rss.getText()
					.toString()));
			nameValuePairs.add(new BasicNameValuePair("x", rx
					.getSelectedItem().toString()));
			nameValuePairs.add(new BasicNameValuePair("y", ry
					.getSelectedItem().toString()));
			nameValuePairs.add(new BasicNameValuePair("device", "SMARTPHONE"));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);
			Toast.makeText(PrincipalActivity.current.getApplicationContext(),
					"Reference Point gravado com Sucesso!", Toast.LENGTH_LONG)
					.show();

			resetRSS();

			// int selection = reference_point.getSelectedItemPosition();

			// if (selection == 99){
			// selection = 0;
			// }else{
			// selection = selection + 1;
			// }
			// reference_point.setSelection(selection);

			// ToneGenerator toneG = new ToneGenerator(
			// AudioManager.STREAM_ALARM, 100);
			// toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 1000);

		} catch (Exception e) {
			Toast.makeText(PrincipalActivity.current.getApplicationContext(),
					"Ocorreu uma falha na comunicação com o servidor!",
					Toast.LENGTH_LONG).show();
		}
	}

	public void gravar() {

		// wifiManager.setWifiEnabled(true);

		// try {
		// Thread.sleep(2000);
		// } catch (InterruptedException e2) {
		// // TODO Auto-generated catch block
		// e2.printStackTrace();
		// }

		if (PrincipalActivity.lista.size() != QTD_COLETA) {
			Toast.makeText(
					PrincipalActivity.current.getApplicationContext(),
					"Existem AP's não coletados, por favor, atualize os dados!",
					Toast.LENGTH_LONG).show();
			ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM,
					100);
			toneG.startTone(ToneGenerator.TONE_SUP_ERROR, 3000);
		}

		for (ReferencePoint rp : PrincipalActivity.lista) {

			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(SERVER);

			boolean success = false;

			do {
				try {
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
							2);
					nameValuePairs.add(new BasicNameValuePair("ap1", String
							.valueOf(rp.getAp1())));
					nameValuePairs.add(new BasicNameValuePair("ap2", String
							.valueOf(rp.getAp2())));
					nameValuePairs.add(new BasicNameValuePair("ap3", String
							.valueOf(rp.getAp3())));
					nameValuePairs.add(new BasicNameValuePair("ap4", String
							.valueOf(rp.getAp4())));
					nameValuePairs.add(new BasicNameValuePair("x", String
							.valueOf(rp.getX())));
					nameValuePairs.add(new BasicNameValuePair("y", String
							.valueOf(rp.getY())));
					nameValuePairs.add(new BasicNameValuePair("device", "SMARTPHONE"));
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					HttpResponse response = httpclient.execute(httppost);
					success = true;
				} catch (Exception e) {
					ToneGenerator toneG = new ToneGenerator(
							AudioManager.STREAM_ALARM, 100);
					toneG.startTone(ToneGenerator.TONE_SUP_ERROR, 1000);
					Toast.makeText(PrincipalActivity.current,
							"Ocorreu uma falha na comunicação com o servidor!",
							Toast.LENGTH_SHORT).show();
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			} while (success == false);
		}

		ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
		toneG.startTone(ToneGenerator.TONE_CDMA_ONE_MIN_BEEP, 500);

		Toast.makeText(PrincipalActivity.current.getApplicationContext(),
				"Reference Point's gravados com Sucesso!", Toast.LENGTH_LONG)
				.show();
		
//		Spinner reference_point = (Spinner) PrincipalActivity.current.findViewById(R.id.reference_point);
//		int selection = reference_point.getSelectedItemPosition();
//
//		if (selection == 200){
//			selection = 0;
//		}else{
//			selection = selection + 1;
//		}
//		reference_point.setSelection(selection);

		PrincipalActivity.lista.clear();
	}
}
