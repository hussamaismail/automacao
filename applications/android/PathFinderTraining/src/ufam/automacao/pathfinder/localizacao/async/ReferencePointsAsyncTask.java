package ufam.automacao.pathfinder.localizacao.async;

import java.util.List;

import ufam.automacao.pathfinder.localizacao.R;
import ufam.automacao.pathfinder.localizacao.activity.PrincipalActivity;
import ufam.automacao.pathfinder.localizacao.domain.ReferencePoint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.widget.Spinner;

public class ReferencePointsAsyncTask extends AsyncTask<Context, Void, Void> {
	
	private WifiManager wifiManager;
	private Context context;
	private boolean finished;
	private ReferencePoint rp;
	
	public ReferencePointsAsyncTask(WifiManager wifiManager, Context context) {
		super();
		this.wifiManager = wifiManager;
		this.context = context;
		this.rp = new ReferencePoint();
	}

	@Override
	protected Void doInBackground(Context... params) {
		
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		
		while(rp.getAp1() == null || rp.getAp2() == null || rp.getAp3() == null || rp.getAp4() == null){
			finished = false;
			
			BroadcastReceiver bcr = new BroadcastReceiver() {
				@Override
				public void onReceive(Context c, Intent intent) {
					Spinner rx = (Spinner) PrincipalActivity.current.findViewById(R.id.x);
					Spinner ry = (Spinner) PrincipalActivity.current.findViewById(R.id.y);
					rp.setX(Integer.valueOf((String)rx.getSelectedItem()));
					rp.setY(Integer.valueOf((String)ry.getSelectedItem()));
					List<ScanResult> results = wifiManager.getScanResults();
					for (ScanResult s : results) {
						if ((s.SSID.equals(PrincipalActivity.AP1)) || (s.SSID.equals(PrincipalActivity.AP2))
								|| (s.SSID.equals(PrincipalActivity.AP3)) || (s.SSID.equals(PrincipalActivity.AP4))) {
							if (s.SSID.equals(PrincipalActivity.AP1)) {
								rp.setAp1(s.level);
							} else if (s.SSID.equals(PrincipalActivity.AP2)) {
								rp.setAp2(s.level);
							} else if (s.SSID.equals(PrincipalActivity.AP3)) {
								rp.setAp3(s.level);
							} else if (s.SSID.equals(PrincipalActivity.AP4)) {
								rp.setAp4(s.level);
							}
						}
					}
					finished = true;
					PrincipalActivity.instance.unregisterReceiver(this);
				}			
			};
			
			context.registerReceiver(bcr,new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
			wifiManager.startScan();
			
			while(finished == false){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		PrincipalActivity.lista.add(rp);
		PrincipalActivity.counter++;
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {				
		PrincipalActivity.concluir();		
		super.onPostExecute(result);
	}

}
