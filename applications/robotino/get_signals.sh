#!/bin/bash
# 
# Script localizacao do Robotino
#
#               Universidade Federal do Amazonas - UFAM
# Author:       Hussama Ismail <hussamaismail@gmail.com>
#
# ------------------------------------------------------
# 
# History:
#
#  v0.1 2014-08-07, Hussama Ismail: 
#     - Initial Version
#

AP1="R-TOP";
AP2="R-BOTTOM";
AP3="R-LEFT";
AP4="R-RIGHT";

INTERFACE="ra0";
TEMPORARY_FILE="/tmp/wifi-scan-result.log";
APS=( $AP1 $AP2 $AP3 $AP4 );
SIGNALS=();
URL="http://172.26.201.2:8080/path-finder/web/reference-point/online/"
WLAN="R-MASTER"

function CHECK_CONNECTION(){
   	if [ $(ifconfig ra0 | grep -i 'inet addr' | wc -l) -eq 0 ]; then
#		echo "[INFO] Configurando dispositivo de rede"
		ifconfig $INTERFACE up;
		iwconfig $INTERFACE essid $WLAN;
		dhclient $INTERFACE;	
	fi
}

#echo "";
#echo "*******************************";
#echo "* ROBOTINO - Localizator v0.1 *";
#echo "*******************************";

#echo "";
while : ; do

#	CHECK_CONNECTION

#        echo "[INFO] Buscando informações de potência do sinal dos AP's: ${APS[@]}";
        SIGNALS=()
	ifconfig $INTERFACE down;
	ifconfig $INTERFACE up;
	sleep 2;
	iwlist $INTERFACE scan > $TEMPORARY_FILE;
	sleep 1;	
	iwlist $INTERFACE scan > $TEMPORARY_FILE;	
	for ap in ${APS[@]}
	do
		FIM=$(cat $TEMPORARY_FILE | grep -n "ESSID:\"$ap\"" | cut -d ":" -f1);
		FIM=$((FIM+3))
		INFO=$(cat $TEMPORARY_FILE | head -n $FIM | tail -n 6);
		SIGNAL=$(echo $INFO | cut -d " " -f15 | cut -d "=" -f2);
		SIGNALS+=($SIGNAL );
	done

	
          
	if [ ${#SIGNALS[@]} -lt 4 ]; then
#  	echo ${SIGNALS[@]};
#		echo "[ERROR] Nao foi possivel obter todas as informacoes. Tentando Novamente ...";
		sleep 2;
		continue;
        fi 

#	echo "[INFO] Coleta efetuada com sucesso: ${SIGNALS[@]}"
  	echo ${SIGNALS[@]};
	RESULT_FILE="/tmp/result-wifi.log";
	break;
#	echo 1 > $RESULT_FILE;

#	while [ : ]; do

# 		if [ $(cat $RESULT_FILE | wc -l) -eq 0 ]; then
#		   break;
#		else
#		   echo "[INFO] Tentando enviar dados para o servidor"
#		   CHECK_CONNECTION
#		fi
		
#		curl --url $URL --data "ap1=${SIGNALS[0]}&ap2=${SIGNALS[1]}&ap3=${SIGNALS[2]}&ap4=${SIGNALS[3]}&device=ROBOTINO" 2> $RESULT_FILE;

#	done

#	echo $SIGNALS
#	echo "[INFO] Dados enviados com sucesso!"
done
