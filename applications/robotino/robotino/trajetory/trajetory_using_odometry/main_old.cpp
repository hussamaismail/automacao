#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <cstdlib>
#include "rec/robotino/com/all.h"
#include "rec/core_lt/utils.h"
#include "rec/core_lt/Timer.h"

#define FORWARD_VELOCITY 200
#define ROTATION_VELOCITY 100
#define IRSIGNAL_TO_ROTATE 1.2
#define NEXTTO	0

int xrobot=120;
int yrobot=120;
int xgoal=120;
int ygoal=180;

int run=30;

using namespace rec::robotino::com;

class MyCom : public Com
{
public:
        MyCom()
        {
        }

        void errorEvent( Error error, const char* errorString )
        {
                std::cerr << "Error: " << errorString << std::endl;
        }

        void connectedEvent()
        {
                std::cout << "Connected." << std::endl;
        }

        void connectionClosedEvent()
        {
                std::cout << "Connection closed." << std::endl;
        }
};

MyCom com;
Motor motor1;
Motor motor2;
Motor motor3;
OmniDrive omniDrive;
Odometry odometry;
Bumper bumper;
DistanceSensor distance0;
DistanceSensor distance1;
DistanceSensor distance8;

void init( const std::string& hostname )
{
        // Initialize the actors
        motor1.setComId( com.id() );
        motor1.setMotorNumber( 0 );

        motor2.setComId( com.id() );
        motor2.setMotorNumber( 1 );

        motor3.setComId( com.id() );
        motor3.setMotorNumber( 2 );

        omniDrive.setComId( com.id() );
        bumper.setComId( com.id() );

        distance0.setComId( com.id() );
        distance0.setSensorNumber( 0 );

        distance1.setComId( com.id() );
        distance1.setSensorNumber( 1 );

	distance8.setComId( com.id() );
        distance8.setSensorNumber( 8 );

	odometry.setComId( com.id() );
	odometry.set(0,0,0);

        // Connect
        std::cout << "Connecting..." << std::endl;
        com.setAddress( hostname.c_str() );

        com.connect();

        std::cout << std::endl << "Connected" << std::endl;
}

void move_right()
{
	omniDrive.setVelocity( 0, FORWARD_VELOCITY, 0);
}

void move_left()
{
	omniDrive.setVelocity( 0, (-1)*FORWARD_VELOCITY, 0);
}


void drive()
{
        rec::core_lt::Timer timer;
        timer.start();

        bool rotating = false;
        unsigned int msecsToRotate = 0;
	float rx = 999;
	float ry = 999;
        
	while (rx != 0 || ry != 0){

	    rx = xgoal - xrobot;
	    ry = ygoal - yrobot;

	    std::cout << "RX: " << rx << std::endl;
	    std::cout << "RY: " << ry << std::endl;

	    if (rx > 0){
	 	odometry.set(0,0,0);
                com.waitForUpdate();
                while(com.isConnected() && odometry.x() < (run * 10)){
                    omniDrive.setVelocity(100, 0, 0 );
                    com.waitForUpdate();
                }
                xrobot = xrobot + run;
		break;
            }else if (rx < 0){
	 	odometry.set(0,0,0);
                com.waitForUpdate();
                while(com.isConnected() && abs(odometry.x()) < (run * 10)){
                    omniDrive.setVelocity(-100, 0, 0 );
                    com.waitForUpdate();
                }
                xrobot = xgoal;
                break;
            }

	    if (ry > 0){
                odometry.set(0,0,0);
                com.waitForUpdate();
                while(com.isConnected() && odometry.y() < (run * 10)){
                    omniDrive.setVelocity(0, 100, 0 );
                    com.waitForUpdate();
                }
                yrobot = ygoal;
		std::cout << "X: " << odometry.x() << " - Y: " << odometry.y() << " - PHI: " << odometry.phi() << std::endl;			
		std::cout << "odometry x: " << odometry.x() << std::endl;
		while(com.isConnected() && abs(odometry.x()) < abs(ajuste)){
		    omniDrive.setVelocity(-100, 0, 0 );
                    com.waitForUpdate();
                }	
		std::cout << "X: " << odometry.x() << " - Y: " << odometry.y() << " - PHI: " << odometry.phi() << std::endl; */
                break;
            }else if (ry < 0){
                odometry.set(0,0,0);
                com.waitForUpdate();
                while(com.isConnected() && abs(odometry.y()) < (run * 10)){
                    omniDrive.setVelocity(0, -100, 0 );
                    com.waitForUpdate();
                }
                yrobot = ygoal;
                break;
            }

  //          if (ry > 0){
////		yrobot = ygoal;	
//		continue;	
//	    }

	   /*else if (rx < 0){
		while(com.isConnected() && timer.msecsElapsed() < 1000 ){
                    omniDrive.setVelocity(0, -100, 0 );
                    com.waitForUpdate();
                }
                yrobot = yrobot - 10;
                timer.reset();
                continue;
	    }*/

/*	    if (rx > 0){ 
	        while( com.isConnected() && timer.msecsElapsed() < 1000 ){
	            omniDrive.setVelocity(100 , 0, 0 );
	            com.waitForUpdate();
	        }
		xrobot = xrobot + 10;
		timer.reset();
		continue;
	    }else if(ry < 0){
		while( com.isConnected() && timer.msecsElapsed() < 1000 ){
                    omniDrive.setVelocity(-100 , 0, 0 );
                    com.waitForUpdate();
                }
                xrobot = xrobot - 10;
                timer.reset();
                continue;
	    } */
	}


/*        while( com.isConnected() && false == bumper.value() )
        {
                if( false == rotating )
                {
                        if( distance0.voltage() < IRSIGNAL_TO_ROTATE
                                && distance1.voltage() < IRSIGNAL_TO_ROTATE
                                && distance8.voltage() < IRSIGNAL_TO_ROTATE )
                        {
                                rx = abs(xgoal - xrobot);
				ry = abs(ygoal - yrobot);

				if (rx<NEXTTO){
					std::cout << "RX";
					omniDrive.setVelocity( FORWARD_VELOCITY, 0, 0 );
					
				}
				
			
				if (ry<NEXTTO){
					std::cout << "RY";
					omniDrive.setVelocity( 0, FORWARD_VELOCITY, 0 );
				}								
			}
	                else
                        {
                                rotating = true;
                                msecsToRotate = rand() % 1000 + 200;
                                timer.start();
                        }}

                else
                {
                        if( timer.msecsElapsed() > msecsToRotate )
                        {
                                rotating = false;
                        }
                        else
                        {
                                omniDrive.setVelocity( 0, FORWARD_VELOCITY, 0);
                        }
                }

                com.waitForUpdate();
        } */
}

void destroy()
{
        com.disconnect();
}

int main( int argc, char **argv )
{
        std::string hostname = "172.26.1.1";
        if( argc > 1 )
        {
                hostname = argv[1];
        }

        xrobot = std::atoi(argv[2]);
	yrobot = std::atoi(argv[3]);
        xgoal = std::atoi(argv[4]);
        ygoal = std::atoi(argv[5]);
        
	try
        {
                init( hostname );
                drive();
                destroy();
        }
        catch( const rec::robotino::com::ComException& e )
        {
                std::cerr << "Com Error: " << e.what() << std::endl;
        }
        catch( const std::exception& e )
        {
                std::cerr << "Error: " << e.what() << std::endl;
        }
        catch( ... )
        {
                std::cerr << "Unknow Error" << std::endl;
        }

        //std::cout << "Press any key to exit..." << std::endl;
        //rec::core_lt::waitForKey();
}


