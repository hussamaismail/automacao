//  Copyright (C) 2004-2008, Robotics Equipment Corporation GmbH

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <cstdlib>

#include "rec/robotino/com/all.h"
#include "rec/core_lt/utils.h"
#include "rec/core_lt/Timer.h"

#define FORWARD_VELOCITY 200
#define ROTATION_VELOCITY 100
#define IRSIGNAL_TO_ROTATE 1.2

using namespace rec::robotino::com;

class MyCom : public Com
{
public:
	MyCom()
	{
	}

	void errorEvent( Error error, const char* errorString )
	{
		std::cerr << "Error: " << errorString << std::endl;
	}

	void connectedEvent()
	{
		std::cout << "Connected." << std::endl;
	}

	void connectionClosedEvent()
	{
		std::cout << "Connection closed." << std::endl;
	}
};

MyCom com;
Motor motor1;
Motor motor2;
Motor motor3;
OmniDrive omniDrive;
Bumper bumper;
DistanceSensor distance0;
DistanceSensor distance1;
DistanceSensor distance8;

void init( const std::string& hostname )
{
	// Initialize the actors
	motor1.setComId( com.id() );
	motor1.setMotorNumber( 0 );

	motor2.setComId( com.id() );
	motor2.setMotorNumber( 1 );

	motor3.setComId( com.id() );
	motor3.setMotorNumber( 2 );

	omniDrive.setComId( com.id() );

	bumper.setComId( com.id() );

	distance0.setComId( com.id() );
	distance0.setSensorNumber( 0 );

	distance1.setComId( com.id() );
	distance1.setSensorNumber( 1 );

	distance8.setComId( com.id() );
	distance8.setSensorNumber( 8 );

	// Connect
	std::cout << "Connecting..." << std::endl;
	com.setAddress( hostname.c_str() );

	com.connect();

	std::cout << std::endl << "Connected" << std::endl;
}

void drive()
{
	rec::core_lt::Timer timer;
	timer.start();

	bool rotating = true;
	unsigned int msecsToRotate = 1000;

	while( com.isConnected() && timer.msecsElapsed() < 1000 ){
//	{
/*		if( false == rotating )
		{
			if( distance0.voltage() < IRSIGNAL_TO_ROTATE
				&& distance1.voltage() < IRSIGNAL_TO_ROTATE
				&& distance8.voltage() < IRSIGNAL_TO_ROTATE )
			{
				omniDrive.setVelocity( FORWARD_VELOCITY, 0, 0 );
			}
			else
			{
				rotating = true;
				msecsToRotate = rand() % 1000 + 200;
				//msecsToRotate = 1000;

				timer.start();
			}
		}
		else
		{
			if( timer.msecsElapsed() > msecsToRotate )
			{
				rotating = false;
			}
			else
			{
				omniDrive.setVelocity( 0, 0, ROTATION_VELOCITY );
			}
		} */


		omniDrive.setVelocity( 0, 0, -15 );
		com.waitForUpdate();
}
//	}
}

void destroy()
{
	com.disconnect();
}

int main( int argc, char **argv )
{
	std::string hostname = "172.26.1.1";
	if( argc > 1 )
	{
		hostname = argv[1];
	}

	try
	{
		init( hostname );
		drive();
		destroy();
	}
	catch( const rec::robotino::com::ComException& e )
	{
		std::cerr << "Com Error: " << e.what() << std::endl;
	}
	catch( const std::exception& e )
	{
		std::cerr << "Error: " << e.what() << std::endl;
	}
	catch( ... )
	{
		std::cerr << "Unknow Error" << std::endl;
	}

	//std::cout << "Press any key to exit..." << std::endl;
	//rec::core_lt::waitForKey();
}
