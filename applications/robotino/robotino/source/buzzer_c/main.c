
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "rec/robotino/com/c/DigitalOutput.h"
#include "rec/robotino/com/c/Com.h"

#ifdef WIN32
#include <windows.h>
// _getch
#include <conio.h>
#else
// getchar
#include <stdio.h>
// usleep
#include <unistd.h>
#endif

ComId com;
DigitalOutputId dout;

//CameraId camera;
//BumperId bumper;

void msleep( unsigned int ms )
{
#ifdef WIN32
	SleepEx( ms, FALSE );
#else
	usleep( ms * 1000 );
#endif
}

void waitForKey()
{
#ifdef WIN32
        _getch();
#else
        getchar();
#endif
}


int main( int argc, char **argv )
{
	com = Com_construct();

	if( argc > 1 )
	{
		Com_setAddress( com, argv[1] );
	}
	else
	{

		Com_setAddress( com, "127.0.0.1" );
		//Com_setAddress( com, "172.26.1.1" );
		//Com_setAddress( com, "192.168.101.101" );
		//Com_setAddress( com, "192.168.3.2:8080" );
	}

	if( FALSE == Com_connect( com ) )
	{
		error( "Error on connect" );
	}
	else
	{
		char addressBuffer[256];
		Com_address( com, addressBuffer, 256 );
		printf( "Connected to %s\n", addressBuffer );
	}

	dout = DigitalOutput_construct(0);
	DigitalOutput_setComId(dout, com);
	DigitalOutput_setValue(dout, 0);

	printf( "Press any key to exit...\n" );
	waitForKey();

	DigitalOutput_destroy(dout);
	Com_destroy(com);


}
