//  Copyright (C) 2004-2008, Robotics Equipment Corporation GmbH

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>

#include "rec/robotino/com/all.h"
#include "rec/core_lt/utils.h"
#include "rec/core_lt/Timer.h"
#include "rec/robotino/com/DigitalOutput.h"

using namespace rec::robotino::com;

class MyCom : public Com
{
public:
	MyCom()
	{
	}

	void errorEvent( Error error, const char* errorString )
	{
		std::cerr << "Error: " << errorString << std::endl;
	}

	void connectedEvent()
	{
		std::cout << "Connected." << std::endl;
	}

	void connectionClosedEvent()
	{
		std::cout << "Connection closed." << std::endl;
	}
};

MyCom com;
OmniDrive omniDrive;
Bumper bumper;
DigitalOutput dout;

void init( const std::string& hostname )
{
	// Connect
	std::cout << "Connecting..." << std::endl;
	com.setAddress( hostname.c_str() );
	com.connect();
	std::cout << std::endl << "Connected: " << com.id() << std::endl;
}

void buzz()
{
	rec::core_lt::Timer timer;
	timer.start();
	DigitalOutput dz;
	dz.setComId(com.id());
	dz.setOutputNumber(0);
	while( com.isConnected()
		&& timer.msecsElapsed() < 100 ){
		dz.setValue(true); 
	}
	while( com.isConnected()
		&& timer.msecsElapsed() < 200 ){
		dz.setValue(false); 
	}
	while( com.isConnected()
		&& timer.msecsElapsed() < 250 ){
		dz.setValue(true); 
	}
	dz.setValue(false); 
}

void destroy()
{
	com.disconnect();
}

int main( int argc, char **argv )
{
	std::cout << "example_forward" << std::endl;

	std::string hostname = "127.0.0.1";
	if( argc > 1 )
	{
		hostname = argv[1];
	}

	try
	{
		init( hostname );
		buzz();
		destroy();
	}
	catch( const rec::robotino::com::ComException& e )
	{
		std::cerr << "Com Error: " << e.what() << std::endl;
	}
	catch( const std::exception& e )
	{
		std::cerr << "Error: " << e.what() << std::endl;
	}
	catch( ... )
	{
		std::cerr << "Unknow Error" << std::endl;
	}
}
