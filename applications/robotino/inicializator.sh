#!/bin/bash

INTERFACE="ra0";
WLAN="R-MASTER"

function CHECK_CONNECTION(){
   	if [ $(ifconfig ra0 | grep -i 'inet addr' | wc -l) -eq 0 ]; then
		echo "[INFO] Configurando dispositivo de rede"
		ifconfig $INTERFACE up;
		iwconfig $INTERFACE essid $WLAN;
		dhclient $INTERFACE;	
	else
		echo "success"
	fi
}

CHECK_CONNECTION

