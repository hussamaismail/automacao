#!/bin/bash

INTERFACE="ra0";
WLAN="R-MASTER"

function CHECK_CONNECTION(){
	IS_INTERFACE_ACTIVE=$(ifconfig | grep -i $INTERFACE | wc -l)
	IS_RUNNING_TRAINING=$(ps -af | grep "training.sh" | wc -l);
	IS_WITHOUT_IP=$(ifconfig $INTERFACE | grep -i 'inet addr' | wc -l);
   	if ([ $IS_INTERFACE_ACTIVE -eq 0 ] || [ $IS_WITHOUT_IP -eq 0 ]) && [ $IS_RUNNING_TRAINING -eq 0 ]; then
		echo "[INFO] Configurando dispositivo de rede"
		ifconfig $INTERFACE up;
		iwconfig $INTERFACE essid $WLAN;
		dhclient $INTERFACE;	
	fi
}

while [ 1 ]; do
	CHECK_CONNECTION		
	sleep 1;
done


