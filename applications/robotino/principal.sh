#!/bin/bash

FOUND_ZONE=0;
LAST_X_SMARTPHONE=-1;
LAST_Y_SMARTPHONE=-1;
TOLERANCE=30;

echo "";
echo "*******************************";
echo "* Path Finder - Robot Project *";
echo "*******************************";
echo "";

while [ : ]; do

   echo "[INFO] Obtendo sinais de rede do Robotino";
   SIGNALS_ROBOTINO=$(./get_signals.sh);
   AP1=$(echo $SIGNALS_ROBOTINO | cut -d " " -f1);
   AP2=$(echo $SIGNALS_ROBOTINO | cut -d " " -f2);
   AP3=$(echo $SIGNALS_ROBOTINO | cut -d " " -f3);
   AP4=$(echo $SIGNALS_ROBOTINO | cut -d " " -f4);

   echo "[INFO] Buscando posicao X,Y do Robotino";
   POSITION_ROBOTINO=$(curl -L "http://172.26.201.2:8080/path-finder/web/reference-point/online?robotino&ap1=$AP1&ap2=$AP2&ap3=$AP3&ap4=$AP4");
   X_ROBOTINO=$(echo $POSITION_ROBOTINO | cut -d "," -f1);
   Y_ROBOTINO=$(echo $POSITION_ROBOTINO | cut -d "," -f2);

   # Checa se e uma coordenada valida
   if ([ $X_ROBOTINO -eq -1 ] && [ $Y_ROBOTINO -eq -1 ]); then
      echo "[INFO] Nao foi possivel localizar o smartphone, tentando novamente ... ";
      continue;
   fi

   echo "[INFO] Buscando posicao X,Y do Smartphone";
   POSITION_SMARTPHONE=$(curl -L "http://172.26.201.2:8080/path-finder/web/reference-point/online?smartphone");
   X_SMARTPHONE=$(echo $POSITION_SMARTPHONE | cut -d "," -f1);
   Y_SMARTPHONE=$(echo $POSITION_SMARTPHONE | cut -d "," -f2);

   if [ $X_SMARTPHONE -eq -1 ] && [ $Y_SMARTPHONE -eq -1 ]; then
      echo "[INFO] Nao foi possivel localizar o smartphone, utilizando ultima posicao armazenada";
      X_SMARTPHONE=$LAST_X_SMARTPHONE;
      Y_SMARTPHONE=$LAST_Y_SMARTPHONE;
   fi

   echo "[INFO] Posicao (x,y) do smartphone: ($X_SMARTPHONE,$Y_SMARTPHONE)";
   echo "[INFO] Posicao (x,y) do robotino: ($X_ROBOTINO,$Y_ROBOTINO)";

   if ([ $X_ROBOTINO -eq -1 ] || [ $X_SMARTPHONE -eq -1 ]); then
      continue
   fi
   
   RX=$((X_ROBOTINO - X_SMARTPHONE));
   RY=$((Y_ROBOTINO - Y_SMARTPHONE));

   if [ $RX -lt 0 ]; then
      RX=$((RX * -1))
   fi
   if [ $RY -lt 0 ]; then
      RY=$((RY * -1))
   fi

   if ([ $RX -le $TOLERANCE ] && [ $RY -le $TOLERANCE ]); then
      ./robotino/buzzer 127.0.0.1;   
      break;
   fi

## USING ZONE CONCEPT NN
#
#   if ([ $X_ROBOTINO -eq $X_SMARTPHONE ] && [ $Y_ROBOTINO -eq $Y_SMARTPHONE ]);then
#      FOUND_ZONE=$((FOUND_ZONE + 1));
#   else
#      FOUND_ZONE=0;
#   fi
#   if [ $FOUND_ZONE -eq 3 ]; then
#      ./robotino/buzzer 127.0.0.1;
#      break;
#   fi

   ./robotino/trajetory_odometry 127.0.0.1 $X_ROBOTINO $Y_ROBOTINO $X_SMARTPHONE $Y_SMARTPHONE;

   LAST_X_SMARTPHONE=$X_SMARTPHONE;
   LAST_Y_SMARTPHONE=$Y_SMARTPHONE;

done

exit 0;
