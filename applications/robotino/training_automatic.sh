#!/bin/bash
# 
# Script de treinamento para localizacao do robotino
#
#               Universidade Federal do Amazonas - UFAM
# Author:       Hussama Ismail <hussamaismail@gmail.com>
#
# ------------------------------------------------------
# 
# History:
#
#  v0.1 2014-08-08, Hussama Ismail: 
#     - Initial Version
#

AP1="R-TOP";
AP2="R-BOTTOM";
AP3="R-LEFT";
AP4="R-RIGHT";

X=$1;
Y=$2;

INTERFACE="ra0";
TEMPORARY_FILE="/tmp/wifi-scan-result.log";
TRAINING_LOG_FILE="/tmp/training.log";
APS=( $AP1 $AP2 $AP3 $AP4 );
CURRENCT_SIGNALS=();
TOTAL_SIGNALS=();
URL="http://172.26.201.2:8080/path-finder/web/reference-point/";
WLAN="R-MASTER";
SAMPLE_NUMBER=20;

COUNT=0;

for j in $(seq 1 5); do

echo "";
echo "**********************************************";
echo "* ROBOTINO - Training Automatic v0.1 ($X,$Y) *";
echo "**********************************************";
echo "";

while [ : ]; do
  	rm -rf $TRAINING_LOG_FILE;

	if [ $COUNT -ge $SAMPLE_NUMBER ]; then
		break;
	fi	
    
        echo "[INFO] Buscando informacões de potência do sinal dos AP's: ${APS[@]}"
        echo "[INFO] Buscando informacões de potência do sinal dos AP's: ${APS[@]}" >> $TRAINING_LOG_FILE;
	rm -f $TEMPORARY_FILE;
	ifconfig $INTERFACE down;
 	ifconfig $INTERFACE up;
	sleep 2;
	iwlist $INTERFACE scan > $TEMPORARY_FILE;	
	for ap in ${APS[@]}
	do
		FIM=$(cat $TEMPORARY_FILE | grep -n "ESSID:\"$ap\"" | cut -d ":" -f1);
		FIM=$((FIM+3))
		INFO=$(cat $TEMPORARY_FILE | head -n $FIM | tail -n 6);
		SIGNAL=$(echo $INFO | cut -d " " -f15 | cut -d "=" -f2);
		CURRENT_SIGNALS+=($SIGNAL);
	done
          
	if [ ${#CURRENT_SIGNALS[@]} -lt 4 ]; then
		echo "${CURRENT_SIGNALS[@]}"
		echo "[ERROR] Nao foi possivel obter todas as informacoes. Tentando Novamente ...";
		echo "[ERROR] Nao foi possivel obter todas as informacoes. Tentando Novamente ..."; >> $TRAINING_LOG_FILE;
		sleep 2;
		CURRENT_SIGNALS=();
		continue;
        fi 

	echo "[INFO] Coleta $((COUNT+1)) efetuada com sucesso ${SIGNALS[@]}"
	echo "[INFO] Coleta $((COUNT+1)) efetuada com sucesso ${SIGNALS[@]}" >> $TRAINING_LOG_FILE;
	TOTAL_SIGNALS+=(${CURRENT_SIGNALS[@]})
	echo "COLETAS: ${CURRENT_SIGNALS[@]}"
	echo "COLETAS: ${CURRENT_SIGNALS[@]}" >> $TRAINING_LOG_FILE;
  	CURRENT_SIGNALS=();
	COUNT=$((COUNT+1));
done

	EXPECTED_SIGNALS=$((4 * $SAMPLE_NUMBER));
if [ ${#TOTAL_SIGNALS[@]} -eq $EXPECTED_SIGNALS ]; then
	echo "[INFO] Todos os sinais foram coletados com sucesso";
	echo "[INFO] Todos os sinais foram coletados com sucesso" >> $TRAINING_LOG_FILE;
	echo "[INFO] Enviando para o servidor";
	echo "[INFO] Enviando para o servidor" >> $TRAINING_LOG_FILE;
	echo "TOTAL SIGNALS: ${TOTAL_SIGNALS[@]}"	
	SENDER_COUNT=0;
	while [ : ]; do
		if [ $SENDER_COUNT -lt $EXPECTED_SIGNALS ]; then
			
			AP1=${TOTAL_SIGNALS[$SENDER_COUNT]}
			AP2=${TOTAL_SIGNALS[$(($SENDER_COUNT + 1))]}
			AP3=${TOTAL_SIGNALS[$(($SENDER_COUNT + 2))]}
			AP4=${TOTAL_SIGNALS[$((SENDER_COUNT + 3))]}

			echo "AP1: $AP1 - AP2: $AP2 - AP3: $AP3 - AP4: $AP4";				
	
			RESULT_FILE="/tmp/result-wifi.log";
 		        echo 1 > $RESULT_FILE;
		        while [ : ]; do

                		if [ $(cat $RESULT_FILE | wc -l) -eq 0 ]; then
		                   break;
                		else
		                   echo "[INFO] Tentando enviar dados para o servidor"
		                   echo "[INFO] Tentando enviar dados para o servidor" >> $TRAINING_LOG_FILE;
                		fi

                	curl --url $URL --interface eth0 --data "ap1=$AP1&ap2=$AP2&ap3=$AP3&ap4=$AP4&device=ROBOTINO&x=$X&y=$Y" 2> $RESULT_FILE;
			done
			echo "[INFO] Dados enviados com sucesso!"	
			echo "[INFO] Dados enviados com sucesso!" >> $TRAINING_LOG_FILE;
			SENDER_COUNT=$(($SENDER_COUNT + 4));
			CURRENT_SIGNALS=();
		else
			SENDER_COUNT=$(($SENDER_COUNT + 4));
			CURRENT_SIGNALS=();
			TOTAL_SIGNALS=();
			COUNT=0;
		   break;
		fi
	done
else
	echo "[ERROR] Ocorreram falhas durante a coleta, tente novamente";
fi




echo "[INFO] Treinamento ($X,$Y) Finalizado!";

	if [ $j -lt 5 ]; then	
		./robotino/left 127.0.0.1	
		Y=$((Y + 30));
	fi
#	done;
done;

	./robotino/buzzer 127.0.0.1
